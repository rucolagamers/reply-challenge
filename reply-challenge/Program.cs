﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace reply_challenge {

    class Region {
        public int ProviderIdx;
        public int RegionIdx;
        public int NPackages;
        public float PackageCost;
        public List<int> ServiceUnitsPerPackage = new List<int>();
        public List<int> Latencies;

        public Region(int ProviderIdx, int RegionIdx, string line2, string line3) {
            this.ProviderIdx = ProviderIdx;
            this.RegionIdx = RegionIdx;

            string[] header = line2.Split(" ", 3);
            NPackages = Int32.Parse(header[0]);
            PackageCost = float.Parse(header[1]);

            ServiceUnitsPerPackage = (from u in header[2].Split(" ")
                                      where u.Length > 0
                                      select int.Parse(u)).ToList();

            Latencies = (from c in line3.Split(" ")
                         where c.Length > 0
                         select int.Parse(c)).ToList();
        }

        public int Buy (int packages) {
            if (NPackages > packages) {
                NPackages -= packages;
                return packages;
            }
            else {
                int PackagesBought = NPackages;
                NPackages = 0;
                return PackagesBought;
            }
        }

        public float AvgUnitCost {
            get {
                return PackageCost / ServiceUnitsPerPackage.Sum();
            }
        }

        public int PackagesNeeded(List<int> UnitsNeeded) {
            int i = 1;
            bool satisfied;

            do {
                satisfied = true;
                List<int> Assigned = (from unit in ServiceUnitsPerPackage
                                      select i * unit).ToList();
                for (int j = 0; j < UnitsNeeded.Count; j++) {
                    if (UnitsNeeded[j] > Assigned[j]) {
                        satisfied = false;
                        i++;
                        break;
                    }
                }
            } while (!satisfied);

            return i;
        }
    }

    class Provider {
        public int NRegion;
        public List<Region> regions = new List<Region>();

        public Provider(int NRegion) {
            this.NRegion = NRegion;
        }

        public void AddRegion(Region region) {
            regions.Add(region);
        }

    }

    class Project {
        public int BasePenalty;
        public int CountryIdx;
        public List<int> UnitsNeeded;
        public List<int> UnitsAllocated;
        public List<int> UnitsRemaining;
        public Dictionary<Region, int> Allocations;

        public Project(string line) {
            string[] header = line.Split(" ", 3);
            BasePenalty = int.Parse(header[0]);
            CountryIdx = Program.Countries.IndexOf(header[1]);
            UnitsNeeded = (from u in header[2].Split(" ")
                           where u.Length > 0
                           select Int32.Parse(u)).ToList();
            UnitsRemaining = new List<int>(UnitsNeeded);
            UnitsAllocated = new List<int>();
            for (int i = 0; i < Program.SServices; i++ ) { UnitsAllocated.Add(0); }

            Allocations = new Dictionary<Region, int>();
        }

        public void Allocate(int n, Region r) {
            if (n == 0) { return; }

            for (int i = 0; i < r.ServiceUnitsPerPackage.Count; i++) {
                UnitsAllocated[i] += r.ServiceUnitsPerPackage[i] * n;
                UnitsRemaining[i] = Math.Max(0, UnitsNeeded[i] - UnitsAllocated[i]);
            }

            if (!Allocations.ContainsKey(r)) { Allocations[r] = 0; }
            Allocations[r] += n;
        }

        public float Penalty {
            get {
                float sum = 0;
                for (int i = 0; i < Program.SServices; i++) {
                    int ua = UnitsAllocated[i];
                    int un = UnitsNeeded[i];
                    if (un != 0) { sum += ((float)(un - Math.Min(un, ua))) / un; }
                }
                return sum / Program.SServices * BasePenalty;
            }
        }

        public float ProjectFactor {
            get {
                float OAI = OverallAvailabilityIndex;
                float TP = OAI == 0 ? 0 : AverageProjectLatency / OAI * OperationalProjectCost;
                return 1e+9f / (TP + Penalty);
            }
        }

        public float AverageProjectLatency {
            get {
                if (Allocations.Count == 0) { return 0; }
                else {
                    float SumLrUr = (from region in Allocations.Keys
                                     select region.Latencies[CountryIdx] * Allocations[region] * region.ServiceUnitsPerPackage.Sum()
                                     ).Sum();
                    float SumUr = (from region in Allocations.Keys
                                   select Allocations[region] * region.ServiceUnitsPerPackage.Sum()
                                   ).Sum();

                    return SumLrUr / SumUr;
                }
            }
        }

        public float OverallAvailabilityIndex {
            get {
                List<float> As = new List<float>();
                for (int i = 0; i < Program.SServices; i++) {
                    float SumQiSq = MathF.Pow(
                        (
                            from pair in Allocations
                            select pair.Value * pair.Key.ServiceUnitsPerPackage[i]
                        ).Sum(),
                        2
                    );
                    float SumSqQi = (
                        from pair in Allocations
                        select MathF.Pow(pair.Value * pair.Key.ServiceUnitsPerPackage[i], 2)
                    ).Sum();

                    if (SumSqQi != 0) { As.Add(SumQiSq / SumSqQi) ; }
                    else { As.Add(0); }
                }
                return As.Average();
            }
        }

        public float OperationalProjectCost {
            get {
                return (from pair in Allocations
                        select pair.Value * pair.Key.PackageCost).Sum();
            }
        }
    }

    class Program {

        public static int VProvider;
        public static int SServices;
        public static int CCountries;
        public static int PProjects;

        public static List<string> Services;
        public static List<string> Countries;

        public static List<Provider> Providers;
        public static List<Region> Regions;
        public static List<Project> Projects;

        const float alpha = 0.5f;
        const int bestPicks = 10;
        const float powbase = 2f;

        private static Random random = new Random(0);

        static void Main(string[] args) {
            if (args.Length < 2) { return; }

            string InFile = args[0];
            string OutFile = args[1];

            StreamReader InStream = File.OpenText(InFile);

            string[] header = InStream.ReadLine().Split(" ");

            VProvider = Int32.Parse(header[0]);
            SServices = Int32.Parse(header[1]);
            CCountries = Int32.Parse(header[2]);
            PProjects = Int32.Parse(header[3]);

            Services = new List<string>(InStream.ReadLine().Split(" "));
            Countries = new List<string>(InStream.ReadLine().Split(" "));

            Providers = new List<Provider>();
            Regions = new List<Region>();

            for (int i = 0; i < VProvider; i++) {
                string[] ProviderHeader = InStream.ReadLine().Split(" ");
                Provider p = new Provider(int.Parse(ProviderHeader[1]));

                for (int j = 0; j < p.NRegion; j++) {
                    InStream.ReadLine();
                    Region r = new Region(i, j, InStream.ReadLine(), InStream.ReadLine());
                    p.AddRegion(r);
                    Regions.Add(r);
                }

                Providers.Add(p);
            }

            Projects = new List<Project>();

            for (int i = 0; i < PProjects; i++) {
                Projects.Add(new Project(InStream.ReadLine()));
            }

            // Start allocating!
            List<Project> SortedProjects = (from p in Projects
                                            orderby p.BasePenalty descending
                                            select p).ToList();

            int PCounter = 0;
            foreach (Project p in SortedProjects) {
                List<Region> SortedRegions = (from r in Regions
                                              where r.NPackages > 0
                                              orderby r.AvgUnitCost * alpha + (1 - alpha) * r.Latencies[p.CountryIdx] ascending
                                              select r).ToList();

                if (SortedRegions.Count == 0) { continue; }

                for (int i = 0; i < Math.Min(bestPicks, SortedRegions.Count) && p.Penalty != 0; i++) {
                    Region r = SortedRegions[i];
                    //float factor = 1 / MathF.Pow(powbase, i+2);
                    float factor = 1 / (float)bestPicks;
                    int requested = (int)Math.Floor(r.NPackages * factor);
                    int bought = r.Buy(requested);
                    p.Allocate(bought, r);
                }

                for (int i = 0; i < Math.Min(bestPicks, SortedRegions.Count) && p.Penalty != 0; i++) {
                    Region r = SortedRegions[i];
                    //float factor = 1 / MathF.Pow(powbase, i+2);
                    float factor = 1 / (float)bestPicks;
                    int requested = (int)Math.Floor(r.NPackages * factor);
                    int bought = r.Buy(requested);
                    p.Allocate(bought, r);
                }

                if (p.Allocations.Count > 0) {
                    Console.Write("Projects assigned: {0:0.00}%\r", ((float)PCounter++ / PProjects) * 100);
                } 
            }
            Console.WriteLine();

            // DO OUTPUT
            // Consider Projects list
            StreamWriter OutStream = File.CreateText(OutFile);
            foreach (Project p in Projects) {
                foreach (KeyValuePair<Region, int> entry in p.Allocations) {
                    OutStream.Write(entry.Key.ProviderIdx + " " + entry.Key.RegionIdx + " " + entry.Value + " ");
                }
                OutStream.Write("\n");
            }
            OutStream.Close();

            float Score = (from project in Projects
                           select project.ProjectFactor).Sum();// * 1e+9f;

            Console.WriteLine("Your score is: {0}", Score);
            Console.ReadKey();
        }
    }
}
