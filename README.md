# Reply Code Challenge!

This is our solution for the Reply Code Challenge 2018. More on this: [click](https://challenges.reply.com).

## Details

Despite the lack of time we still managed to end up with a somewhat working solution, which still awarded us the 50th place over 1200 partecipants. As it is possible to see, most of our time was spent writing support code the reflected the highly complex problem which was given to us. The core assignment algorithm is what we're lacking and is what we would have developed next given more time. Right now we don't plan finishing this, but feel free to give a look around :)

## Partecipants

* [Alessio Giuseppe Cali'](https://www.linkedin.com/in/alessio-giuseppe-cali/)
* [Kevin Corizi](https://www.linkedin.com/in/kevincorizi/)
* [Maria Giulia Canu](https://www.linkedin.com/in/mariagiuliacanu/)
* [Simone Leonardi](https://www.linkedin.com/in/simone-leonardi-42a6536b/)